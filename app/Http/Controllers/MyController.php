<?php

namespace App\Http\Controllers;

use App\Models\Histories;
use App\Models\Narracions;
use App\Models\Partides;
use App\Models\Personatges;
use App\Models\PersonatgesPartides;
use App\Models\Rols;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use function Sodium\add;

class MyController extends Controller
{

    // Aquest constructor verifica que l'usuari hagi accedir amb e-mail i contrasenya
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPartides()
    {
        $userId = Auth::id();
        $usuari = User::where('id', $userId)->first();

        if ($usuari->idRols == 1) { //Totes les partides
            $partides_personatge = PersonatgesPartides::all();
            return Inertia::render('LesMevesPartides', ['partides_personatge' => $partides_personatge]);

        } else if ($usuari->idRols == 2) { //Partides en que es narrador
            $personatges = Personatges::where('idUser', $userId)->pluck('idPersonatge');
            $partides_personatge = PersonatgesPartides::whereIn('idPersonatge', $personatges)->get();
            return Inertia::render('LesMevesPartides', ['partides_personatge' => $partides_personatge]);

        } else { //Partides en que participa el jugador
            $personatges = Personatges::where('idUser', $userId)->pluck('idPersonatge');
            $partides_personatge = PersonatgesPartides::whereIn('idPersonatge', $personatges)->get();
            return Inertia::render('LesMevesPartides', ['partides_personatge' => $partides_personatge]);
        }
    }

    public function getAllUsersAndRols()
    {
        $users = User::all();
        $rols = Rols::all();
        return Inertia::render('LlistarUsers', ['users' => $users, 'rols' => $rols]);
    }

    public function getBeforeAddUser(Request $request)
    {
        $users = User::all();
        return Inertia::render('AfegirUsuari', ['users' => $users]);
    }

    public function getBeforeAddHistories()
    {
        $histories = Histories::all();
        return Inertia::render('AfegirHistories', ['histories' => $histories]);
    }

    public function getBeforeAddNarracions()
    {
        $partides = Partides::all()->where('acabada', false)->pluck('idPartida')->toArray();
        return Inertia::render('AfegirNarracions', ['partides' => $partides]);
    }

    public function getBeforeEditHistories()
    {
        $histories = Histories::all();
        return Inertia::render('EditarHistories', ['histories' => $histories]);
    }

    public function getBeforeAddPartides()
    {
        $histories = Histories::all();
        return Inertia::render('AfegirPartides', ['histories' => $histories]);
    }

    public function addPartides(Request $request)
    {
        $partida = new Partides();
        $partida->idHistoria = $request->idHistoria;
        $partida->idGuanyador = $request->idGuanyador;
        $partida->punts = $request->punts;
        $partida->acabada = $request->acabada;
        $partida->save();
        return redirect('/partides');
    }

    public function getBeforeEditNarracions()
    {
        $narracions = Narracions::all();
        $partides = Partides::where('acabada', false)->get()->pluck('idPartida')->toArray();
        return Inertia::render('EditarNarracions', ['narracions' => $narracions, 'partides' => $partides]);
    }

    public function editHistoria(Request $request)
    {
        $historia = Histories::find($request->idHistoria);
        $historia->titol = $request->titol;
        if ($request->fitxer != null) {
            $request->validate([
                //el campo file es obligatorio, debe ser extension pdf xlx o csv, tamaño maximo 2MB
                'fitxer' => 'required|mimes:pdf,txt,doc,docx|max:2048',
            ]);
            $fileName2 = $request->fitxer->getClientOriginalName();
            $request->fitxer->move(public_path('historias'), $fileName2);
            $historia->fitxer = "historias/" . $fileName2;
        } else {
            $historia->fitxer = $request->fitxer;
        }
        $historia->save();
        return redirect('/histories');
    }

    public function editNarracio(Request $request)
    {
        $narracions = Narracions::find($request->idNarracio);
        $narracions->idPartida = $request->idPartida;
        $narracions->titol = $request->titol;
        $narracions->descripcio = $request->descripcio;
        $narracions->realizada = $request->realizada;
        $narracions->mecanicaDau = $request->mecanicaDau;
        $narracions->minimDau = $request->minimDau;
        $narracions->ordre = $request->ordre;
        $narracions->puntsExperiencia = $request->puntsExperiencia;
        $narracions->save();
        return redirect('/narracions');
    }

    public function addHistoria(Request $request)
    {

        $historia = new Histories();
        $historia->titol = $request->titol;
        $historia->idUser = Auth::id();
        if ($request->fitxer != null) {
            $request->validate([
                //el campo file es obligatorio, debe ser extension pdf xlx o csv, tamaño maximo 2MB
                'fitxer' => 'required|mimes:pdf,txt,doc,docx|max:2048',
            ]);
            $fileName2 = $request->fitxer->getClientOriginalName();
            $request->fitxer->move(public_path('historias'), $fileName2);
            $historia->fitxer = "historias/" . $fileName2;
        } else {
            $historia->fitxer = $request->fitxer;
        }
        $historia->save();
        return redirect('/histories');
    }

    public function addNarracio(Request $request)
    {
        $narracions = new Narracions();
        $narracions->idPartida = $request->idPartida;
        $narracions->titol = $request->titol;
        $narracions->descripcio = $request->descripcio;
        $narracions->realizada = $request->realizada;
        $narracions->mecanicaDau = $request->mecanicaDau;
        $narracions->minimDau = $request->minimDau;
        $narracions->ordre = $request->ordre;
        $narracions->puntsExperiencia = $request->puntsExperiencia;
        $narracions->save();
        return redirect('/narracions');
    }

    public function addUser(Request $request)
    {
        $user = new User();
        $user->name = $request->nom;
        $user->email = $request->email;
        $user->password = $request->contrasenya;
        $user->idRols = $request->rol;
        $user->save();
        return redirect('/jugadors');
    }

    public function getBeforeEditUser()
    {
        $users = User::all();
        return Inertia::render('EditarUsuari', ['users' => $users]);
    }

    public function editUser(Request $request)
    {
        $user = User::find($request->selectedOption);
        $user->name = $request->nom;
        $user->email = $request->email;
        $user->save();
        return redirect('/jugadors');
    }

    public function getAllRolsAndUsers()
    {
        $users = User::all();
        $rols = Rols::all();
        return Inertia::render('LListarRols', ['users' => $users, 'rols' => $rols]);
    }

    public function getAllUsersRols()
    {
        $users = User::all();
        $rols = Rols::all();
        return Inertia::render('Profile', ['users' => $users, 'rols' => $rols]);
    }

    public function editUserRols(Request $request)
    {
        $user = User::find($request->selectedIdUser);
        $user->idRols = $request->selectedIdRols;
        $user->save();
        return redirect('/jugadors');
    }

    public function getAllRolsAdd()
    {
        $rols = Rols::all();
        return Inertia::render('AfegirRol', ['rols' => $rols]);
    }

    public function getAllRolsEdit()
    {
        $rols = Rols::all();
        return Inertia::render('EditarRol', ['rols' => $rols]);
    }

    public function addRol(Request $request)
    {
        $rol = new Rols();
        $rol->nom = $request->nom;
        $rol->save();
        return redirect('/rols');
    }

    public function editRol(Request $request)
    {
        $rol = Rols::find($request->selectedIdRols);
        $rol->nom = $request->selectedNomRols;
        $rol->save();
        return redirect('/rols');
    }

    public function getAllUsersByRol(Request $request)
    {
        $rol = $request->selectedOption;
        $rols = Rols::all();
        $users = User::where('idRols', $rol)->get();

        return Inertia::render('LListarRols', ['users' => $users, 'rols' => $rols]);
    }

    public function paginaAfegirTirada()
    {
        $userId = Auth::id();
        $idPersonatges = Personatges::where('idUser', $userId)->pluck('idPersonatge');
        $personatges = Personatges::whereIn('idPersonatge', $idPersonatges)->get();
        $idsPartides = PersonatgesPartides::where('acabada', false)->distinct('idPartida')->pluck('idPartida');
        return Inertia::render('AfegirTirada', ['personatges' => $personatges, 'idsPartides' => $idsPartides]);
    }

    public function afegirNovaTirada(Request $request)
    {
        $narracio = Narracions::where('realizada', false)->where('idPartida', $request->partidaSeleccionada)->orderBy('ordre', 'asc')->get()->first(); // Importante poner el first porque el where devuelve un array
        if ($narracio == null) {
            return Inertia::render('AccioIncorrecta', ['missatge' => "No hi ha narracions sense realitzar en la partida seleccionada."]);
        }
        $partida = PersonatgesPartides::where('idNarracio', $narracio->idNarracio)->where('idPersonatge', $request->personatgeSeleccionat)->get()->first();

        if ($partida == null) {
            return Inertia::render('AccioIncorrecta', ['missatge' => "Aquest personatge no està en la partida seleccionada. Primer afegeix el teu personatge a la partida per poder afegir una tirada."]);
        }

        $dauRandom = rand(1, 20);
        $partida->ultimaTirada = $dauRandom;
        $partida->idPersonatge = $request->personatgeSeleccionat;
        $partida->save();
        $narracio->personatges_partides()->save($partida);
        return redirect('/');
    }

    public function paginaAfegirPersonatgeAPartida()
    {
        $userId = Auth::id();
        $user = User::where('id', $userId)->first();
        $partides = Partides::where('acabada', false)->get();

        if ($user->idRols == 1) {
            $personatges = Personatges::all();

        } else {
            $personatges = Personatges::where('idUser', $userId)->get();

        }
        return Inertia::render('AfegirPersonatgeAPartida', ['personatges' => $personatges, 'partides' => $partides]);

        //PARA DEBUGAR:
        // dd($personatges); // Log que se muestra en la misma página al cargar
        // Log::info('Personatges: ' . $personatges); // Se muestra en el archivo de logs de Laravel ('laravel.log')
        // Per imprimir arrays en PHP: print_r($idsPersonatgesUser)
    }

    public function afegirPersonatgeAPartida(Request $request)
    {
        //Primer mirem si el jugador te personatges en la partida seleccionada (en el cas de l'administrador, si l'user del personatge seleccionat te personatges en la partida seleccionada)
        $idUser = Auth::id();
        $user = User::where('id', $idUser)->first();

        if ($user->idRols == 1) {
            $idUserPersonatgeSeleccionat = Personatges::where('idPersonatge', $request->personatgeSeleccionat)->select('idUser')->first();
            // Amb la pluck() en retorna els valors de la columna que li diguem i ens retorna en array
            $idsPersonatgesUser = Personatges::whereIn('idUser', $idUserPersonatgeSeleccionat)->get()->pluck('idPersonatge')->toArray();
            $personatgesEnPartida = PersonatgesPartides::whereIn('idPersonatge', $idsPersonatgesUser)->where('idPartida', $request->partidaSeleccionada)->get();
        } else {
            $idsPersonatgesUser = Personatges::where('idUser', $idUser)->get()->pluck('idPersonatge')->toArray();
            $personatgesEnPartida = PersonatgesPartides::whereIn('idPersonatge', $idsPersonatgesUser)->where('idPartida', $request->partidaSeleccionada)->get();
        }

        if (count($personatgesEnPartida) > 0) {
            return Inertia::render('AccioIncorrecta', ['missatge' => "Aquest usuari ja té un personatge en aquesta partida, no es poden afegir més."]);
        }

        $partida_personatge = new PersonatgesPartides();
        $partida_personatge->idPartida = $request->partidaSeleccionada;
        $partida_personatge->idPersonatge = $request->personatgeSeleccionat;
        $partida = Partides::find($request->partidaSeleccionada);
        $partida_personatge->acabada = $partida->acabada;
        $partida_personatge->idHistoria = $partida->idHistoria;
        $narracio = Narracions::where('idPartida', $request->partidaSeleccionada)->first();
        $partida_personatge->idNarracio = $narracio->idNarracio;
        $personatge = Personatges::find($request->personatgeSeleccionat);
        $partida_personatge->punts = $personatge->puntsInicials;
        $partida_personatge->save();
        return redirect('/');
    }

    public function getAllHistories()
    {
        $histories = Histories::all();
        return Inertia::render('Histories', ['histories' => $histories]);
    }

    public function getAllNarracions()
    {
        $userId = Auth::id();

        if ($userId == 1 || $userId == 2) {
            $narracions = Narracions::all();
            return Inertia::render('Narracions', ['narracions' => $narracions]);
        } else {
            $userId = Auth::id();
            $personatges = Personatges::where('idUser', $userId)->pluck('idPersonatge'); // Pluck es para recuperar todos los valores de la columna 'idPersonatge'
            $idsPartides = PersonatgesPartides::whereIn('idPersonatge', $personatges)->get()->pluck('idPartida');

            $narracions = Narracions::whereIn('idPartida', $idsPartides)->get();

            return Inertia::render('NarracionsMevesPartides', ['narracions' => $narracions]);
        }
    }

    public function getResoldreDausNarracions()
    {
        $partides_personatge1 = PersonatgesPartides::where('acabada', 0)->get()->unique();
        return Inertia::render('ResoldreDaus', ['partides_personatge' => $partides_personatge1]);
    }

    public function resoldreDausNarracions(Request $request)
    {

        $narracion = Narracions::where('idPartida', $request->idPartida)->where('realizada', 0)->orderBy('ordre', 'asc')->first();

        if ($narracion->mecanicaDau == "Mínim") {
            $personatgePartida = PersonatgesPartides::where('idPartida', $request->idPartida)->first();
            if ($personatgePartida->ultimaTirada >= $narracion->minimDau) {
                $personatgePartida->punts = $personatgePartida->punts + $narracion->puntsExperiencia;
                $narracion->realizada = true;
                $narracion->save();
                $narracion->personatges_partides()->save($personatgePartida);
            }
        } else if ($narracion->mecanicaDau == "Màxim") {
            $personatgePartida = PersonatgesPartides::where('idPartida', $request->idPartida)->get();
            $dados = $personatgePartida->pluck('ultimaTirada')->toArray();
            $maxim = max($dados);

            if ($maxim > $narracion->minimDau) {

                $personatgeGuanyador = PersonatgesPartides::where('idPartida', $request->idPartida)->where('ultimaTirada', $maxim)->first();
                $personatgeGuanyador->punts = $personatgeGuanyador->punts + $narracion->puntsExperiencia;
                $narracion->realizada = true;
                $narracion->save();
                $narracion->personatges_partides()->save($personatgeGuanyador);
            }
        }
        return redirect('/narracions');
    }

    public function getAllPartidasAcabar()
    {
        $partides = Partides::all();
        return Inertia::render('AcabarPartides', ['partides' => $partides]);
    }

    public function getAllPartides()
    {
        $userId = Auth::id();
        $usuari = User::where('id', $userId)->first();

        if ($usuari->idRols <= 2) { //Totes les partides
            $partides = Partides::all();
            return Inertia::render('Partides', ['partides' => $partides]);
        } else { //Partides en que participa el jugador
            $personatges = Personatges::where('idUser', $userId)->pluck('idPersonatge');
            $partidespersonatges = PersonatgesPartides::whereIn('idPersonatge', $personatges)->get();
            return Inertia::render('Partides', ['partides' => $partidespersonatges]);
        }
    }

    public function acabarPartida(Request $request)
    {
        $partida = Partides::find($request->selectedIdPartida);
        $partida->acabada = 1;
        $partida->save();
        return redirect('/partides/endGame');
    }
}
