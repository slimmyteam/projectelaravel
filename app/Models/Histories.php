<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Histories extends Model
{
    use HasFactory;

    protected $table = "histories";
    protected $primaryKey = "idHistoria";
    protected $fillable = ["idHistoria", "idUser", "titol", "fitxer"];

    //OneToMany
    public function partides()
    {
        return $this->hasMany(Partides::class)->withTimestamps();
    }

    //ManyToOne
    public function users()
    {
        return $this->BelongsTo(User::class, 'id', 'idUser')->withTimestamps();
    }
}
