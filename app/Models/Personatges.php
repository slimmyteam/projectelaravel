<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personatges extends Model
{
    use HasFactory;

    protected $table = "personatges";
    protected $primaryKey = "idPersonatge";
    protected $fillable = ["idPersonatge", "idTipus", "idPartida", "nom", "puntsInicials", "rerefons", "nivell", "rasa", "militancia", "presentacio", "foto"];

    //ManyToOne
    public function users()
    {
        return $this->BelongsTo(User::class, 'idPersonatge', 'idPersonatge')->withTimestamps();
    }

    //ManyToOne
    public function tipus()
    {
        return $this->belongsTo(Tipus::class, 'idTipus', 'idTipus')->withTimestamps();
    }

    /*//OneToMany
    public function partides()
    {
        return $this->hasMany(Partides::class);
    }

    //ManyToMany
    public function partidesManyToMany()
    {
        return $this->belongsToMany(Partides::class, 'partida_personatge', 'idPersonatge', 'idPartida');
    }*/

    public function PersonatgesPartides()
    {
        return $this->belongsToMany(Partides::class, 'personatges_partides', 'idGuanyador','idPersonatge')->using(PersonatgesPartides::class)->withTimestamps();
    }
}
