<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipus extends Model
{
    use HasFactory;

    protected $table = "tipus";
    protected $primaryKey = "idTipus";
    protected $fillable = ["idTipus", "nom", "descripcio"];

    //OneToMany
    public function personatges()
    {
        return $this->hasMany(Personatges::class)->withTimestamps();
    }
}
