<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PersonatgesPartides extends Pivot
{
    use HasFactory;

    protected $table = "personatges_partides";
    protected $primaryKey = "idPersonatgePartida";
    protected $fillable = ["idPersonatgePartida", "idPartida", "idHistoria","idNarracio", "idPersonatge",  "punts", "ultimaTirada", "acabada"];

    //ManyToOne
    public function narracions()
    {
        return $this->BelongsTo(Narracions::class, 'idNarracio', 'idNarracio')->withTimestamps();
    }
}
