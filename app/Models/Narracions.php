<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Narracions extends Model
{
    use HasFactory;

    protected $table = "narracions";
    protected $primaryKey = "idNarracio";
    protected $fillable = ["idNarracio", "idPartida", "titol", "descripcio", "realizada", "mecanicaDau", "minimDau", "ordre", "puntsExperiencia"];

    //ManyToOne
    public function partides()
    {
        return $this->BelongsTo(Partides::class, 'idPartida', 'idPartida')->withTimestamps();
    }

    //OneToMany
    public function personatges_partides()
    {
        return $this->hasMany(PersonatgesPartides::class,'idNarracio','idNarracio');
    }
}
