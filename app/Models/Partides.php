<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partides extends Model
{
    use HasFactory;

    protected $table = "partides";
    protected $primaryKey = "idPartida";
    protected $fillable = ["idPartida", "idHistoria", "idGuanyador", "punts", "acabada"];

    //ManyToOne
    public function histories()
    {
        return $this->BelongsTo(Histories::class, 'idHistoria', 'idHistoria')->withTimestamps();
    }

    //OneToMany
    public function narracions()
    {
        return $this->hasMany(Narracions::class)->withTimestamps();
    }

    /*//ManyToOne
    public function personatges()
    {
        return $this->BelongsTo(Personatges::class, 'idPersonatge', 'idGuanyador')->withTimestamps();
    }

    //ManyToMany
    public function personatgesManyToMany()
    {
        return $this->belongsToMany(Personatges::class, 'partida_personatge', 'idPartida', 'idPersonatge')->withTimestamps();
    }*/

    public function PersonatgesPartides()
    {
        return $this->belongsToMany(Personatges::class, 'personatges_partides', 'idPartida','idPartida')->using(PersonatgesPartides::class)->withTimestamps();
    }
}
