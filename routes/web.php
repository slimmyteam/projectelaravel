<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/SensePermisos', function () {
    return Inertia::render('SensePermisos');
})->middleware('auth')->name('SensePermisos');

Route::get('/Profile', [\App\Http\Controllers\MyController::class, 'getAllUsersRols'])->middleware('auth.administrador')->name('editarRolsUsuaris');
Route::post('/Profile', [\App\Http\Controllers\MyController::class, 'editUserRols'])->name('editarRolsUsuarisPost');

Route::get('/', [\App\Http\Controllers\MyController::class, 'getPartides'])->name('llistaPartides');

Route::get('/jugadors', [\App\Http\Controllers\MyController::class, 'getAllUsersAndRols'])->name('llistaUsuaris');
Route::get('/jugadors/add', [\App\Http\Controllers\MyController::class, 'getBeforeAddUser'])->name('getAfegirUsuari');
Route::post('/jugadors/add', [\App\Http\Controllers\MyController::class, 'addUser'])->name('afegirUsuari');
Route::get('/jugadors/edit', [\App\Http\Controllers\MyController::class, 'getBeforeEditUser'])->name('getEditarUsuari');
Route::post('/jugadors/edit', [\App\Http\Controllers\MyController::class, 'editUser'])->name('editarUsuari');

Route::get('/partides', [\App\Http\Controllers\MyController::class, 'getAllPartides'])->name('llistarPartides');
Route::get('/partides/idJugador', [\App\Http\Controllers\MyController::class, 'paginaAfegirPersonatgeAPartida'])->name('paginaAfegirPersonatgeAPartida');
Route::post('/partides/idJugador', [\App\Http\Controllers\MyController::class, 'afegirPersonatgeAPartida'])->name('afegirPersonatgeAPartida');
Route::get('/partides/add', [\App\Http\Controllers\MyController::class, 'getBeforeAddPartides'])->middleware('auth.narrador')->name('getAfegirPartida');
Route::post('/partides/add', [\App\Http\Controllers\MyController::class, 'addPartides'])->middleware('auth.narrador')->name('afegirPartida');
Route::get('/partides/endGame', [\App\Http\Controllers\MyController::class, 'getAllPartidasAcabar'])->middleware('auth.administrador')->name('acabarPartida');
Route::post('/partides/endGame', [\App\Http\Controllers\MyController::class, 'acabarPartida'])->name('acabarPartidaPost');

Route::get('/narracions', [\App\Http\Controllers\MyController::class, 'getAllNarracions'])->name('llistaNarracions');
Route::get('/narracions/addRoll', [\App\Http\Controllers\MyController::class, 'paginaAfegirTirada'])->name('paginaAfegirTirada');
Route::post('/narracions/addRoll', [\App\Http\Controllers\MyController::class, 'afegirNovaTirada'])->name('afegirTirada');
Route::get('/narracions/add', [\App\Http\Controllers\MyController::class, 'getBeforeAddNarracions'])->middleware('auth.narrador')->name('getAfegirNarracions');
Route::post('/narracions/add', [\App\Http\Controllers\MyController::class, 'addNarracio'])->name('afegirNarracio');
Route::get('/narracions/edit', [\App\Http\Controllers\MyController::class, 'getBeforeEditNarracions'])->middleware('auth.narrador')->name('getEditarNarracions');
Route::post('/narracions/edit', [\App\Http\Controllers\MyController::class, 'editNarracio'])->name('editarNarracio');
Route::get('/narracions/solveRoll', [\App\Http\Controllers\MyController::class, 'getResoldreDausNarracions'])->middleware('auth.narrador')->name('getResoldreDausNarracions');
Route::post('/narracions/solveRoll', [\App\Http\Controllers\MyController::class, 'resoldreDausNarracions'])->middleware('auth.narrador')->name('resoldreDausNarracions');

Route::get('/histories', [\App\Http\Controllers\MyController::class, 'getAllHistories'])->middleware('auth.narrador')->name('llistaHistories');
Route::get('/histories/add', [\App\Http\Controllers\MyController::class, 'getBeforeAddHistories'])->middleware('auth.narrador')->name('getAfegirHistories');
Route::post('/histories/add', [\App\Http\Controllers\MyController::class, 'addHistoria'])->name('afegirHistoria');
Route::get('/histories/edit', [\App\Http\Controllers\MyController::class, 'getBeforeEditHistories'])->middleware('auth.narrador')->name('getEditarHistories');
Route::post('/histories/edit', [\App\Http\Controllers\MyController::class, 'editHistoria'])->name('editarHistoria');

Route::get('/rols', [\App\Http\Controllers\MyController::class, 'getAllRolsAndUsers'])->middleware('auth.administrador')->name('llistaRols');
Route::get('/rols/add', [\App\Http\Controllers\MyController::class, 'getAllRolsAdd'])->middleware('auth.administrador')->name('getAfegirRol');
Route::get('/rols/edit', [\App\Http\Controllers\MyController::class, 'getAllRolsEdit'])->middleware('auth.administrador')->name('getEditarRol');
Route::post('/rols/add', [\App\Http\Controllers\MyController::class, 'addRol'])->name('addRolPost');
Route::post('/rols/edit', [\App\Http\Controllers\MyController::class, 'editRol'])->name('editRolPost');
Route::post('/rols', [\App\Http\Controllers\MyController::class, 'getAllUsersByRol'])->name('getAllUsersByRol');


require __DIR__ . '/auth.php';

