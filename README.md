# 🐉 Gestión de partidas del juego _Dungeons and Dragons_ 🐉

Página web para gestionar partidas del juego _Dungeons and Dragons_ donde los diferentes usuarios que pueden iniciar sesión podrán visualizar una información u otra dependiendo de su rol. 

Además, podrán añadir/editar partidas, usuarios, personajes y narraciones, entre otros. 
Aplicación en Python que realiza operaciones CRUD sobre una base de datos documental en Mongo.  

## 📄 Descripción
Proyecto realizado para la asignatura "Acceso a datos" desarrollado durante el segundo año de Grado Superior de Desarrollo de Aplicaciones Multiplataforma + Perfil videojuegos y ocio digital (DAMvi).

## 💻  Tecnologías 
![Laravel](https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-4479A1.svg?style=for-the-badge&logo=mysql&logoColor=white)
- Eloquent.
- Patrón de arquitectura: MVC (Modelo-Vista-Controlador).

## ⚙️ Funcionamiento
El funcionamiento se puede ver en el documento de pruebas del proyecto adjuntado a continuación: [Documento de pruebas](https://gitlab.com/slimmyteam/accesodatos/projectelaravel/-/blob/35d8681959ed4a7d58cd3cd752d2a0948bce27bb/Document_de_proves_Laravel.pdf).
