<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('narracions', function (Blueprint $table) {
            $table->id('idNarracio');
            $table->foreignId('idPartida')->constrained('partides')->references('idPartida');
            $table->string('titol',30)->nullable(false);
            $table->text('descripcio')->nullable(false);
            $table->boolean('realizada')->default(false);
            $table->string('mecanicaDau',10)->nullable(false);
            $table->integer('minimDau')->default(1);
            $table->integer('ordre')->default(1);
            $table->integer('puntsExperiencia')->default(10);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('narracions', function (Blueprint $table) {
            $table->dropForeign(['narracions_idPartida_foreign']);
            $table->dropColumn('idPartida');
        });
        Schema::dropIfExists('narracions');
    }
};
