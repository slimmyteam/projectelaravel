<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('personatges_partides', function (Blueprint $table) {
            $table->id('idPersonatgePartida');
            $table->foreignId('idPartida')->constrained('partides')->references('idPartida');
            $table->foreignId('idHistoria')->constrained('histories')->references('idHistoria');
            $table->foreignId('idNarracio')->constrained('narracions')->references('idNarracio');
            $table->foreignId('idPersonatge')->constrained('personatges')->references('idPersonatge');
            $table->integer('punts')->default(0);
            $table->integer('ultimaTirada')->nullable();
            $table->boolean('acabada')->default(false);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('personatges_partides', function (Blueprint $table) {
            $table->dropForeign(['personatges_partides_idPartida_foreign']);
            $table->dropColumn('idPartida');
            $table->dropForeign(['personatges_partides_idHistoria_foreign']);
            $table->dropColumn('idHistoria');
            $table->dropForeign(['personatges_partides_idNarracio_foreign']);
            $table->dropColumn('idNarracio');
            $table->dropForeign(['personatges_partides_idPersonatge_foreign']);
            $table->dropColumn('idPersonatge');

        });
        Schema::dropIfExists('personatges_partides');
    }
};
