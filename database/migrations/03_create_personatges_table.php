<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('personatges', function (Blueprint $table) {
            $table->id('idPersonatge');
            $table->foreignId('idTipus')->constrained('tipus')->references('idTipus');
            $table->foreignId('idUser')->constrained('users')->references('id');
            $table->string('nom',30)->nullable(false);
            $table->integer('puntsInicials')->default(0);
            $table->string('rerefons', 200)->nullable();
            $table->integer('nivell')->default(0);
            $table->string('rasa', 30)->nullable();
            $table->string('militancia', 50)->nullable();
            $table->text('presentacio')->nullable();
            $table->text('foto')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('personatges', function (Blueprint $table) {
            $table->dropForeign(['personatges_idTipus_foreign']);
            $table->dropColumn('idTipus');
            $table->dropForeign(['personatges_idUser_foreign']);
            $table->dropColumn('idUser');
        });
        Schema::dropIfExists('personatges');
    }
};
