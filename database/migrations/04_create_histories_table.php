<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('histories', function (Blueprint $table) {
            $table->id('idHistoria');
            $table->foreignId('idUser')->constrained('users')->references('id');
            $table->string('titol',30)->nullable(false);
            $table->text('fitxer')->nullable(false);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('histories', function (Blueprint $table) {
            $table->dropForeign(['histories_idUser_foreign']);
            $table->dropColumn('idUser');
        });
        Schema::dropIfExists('histories');
    }
};
