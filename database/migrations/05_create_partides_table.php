<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('partides', function (Blueprint $table) {
            $table->id('idPartida');
            $table->foreignId('idHistoria')->constrained('histories')->references('idHistoria');
            $table->foreignId('idGuanyador')->constrained('personatges')->references('idPersonatge');
            $table->integer('punts')->default(0);
            $table->boolean('acabada')->default(false);
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('partides', function (Blueprint $table) {
            $table->dropForeign(['partides_idHistoria_foreign']);
            $table->dropColumn('idHistoria');
            $table->dropForeign(['partides_idGuanyador_foreign']);
            $table->dropColumn('idGuanyador');
        });
        Schema::dropIfExists('partides');
    }
};
