<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NarracionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('narracions')->insert([
            'idPartida' => 1,
            'titol' => "Elfo dispara",
            'descripcio' => "Elfo dispara con un arco",
            'realizada' => true,
            'mecanicaDau' => 'Mínim',
            'minimDau' => 3,
            'ordre' => 1,
            'puntsExperiencia' => 30,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('narracions')->insert([
            'idPartida' => 2,
            'titol' => "Druida explica",
            'descripcio' => "Druida muestra su sabiduría",
            'realizada' => false,
            'mecanicaDau' => 'Màxim',
            'minimDau' => 5,
            'ordre' => 2,
            'puntsExperiencia' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('narracions')->insert([
            'idPartida' => 3,
            'titol' => "Warrior lucha",
            'descripcio' => "Warrior se enfrenta a una serie de malvados orcos",
            'realizada' => true,
            'mecanicaDau' => 'Mínim',
            'minimDau' => 4,
            'ordre' => 3,
            'puntsExperiencia' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
