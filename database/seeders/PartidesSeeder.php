<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('partides')->insert([
            'idHistoria' => 1,
            'idGuanyador' => 1,
            'punts' => 50,
            'acabada' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('partides')->insert([
            'idHistoria' => 2,
            'idGuanyador' => 1,
            'punts' => 25,
            'acabada' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('partides')->insert([
            'idHistoria' => 3,
            'idGuanyador' => 3,
            'punts' => 100,
            'acabada' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
