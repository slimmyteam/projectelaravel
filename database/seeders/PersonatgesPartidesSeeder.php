<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonatgesPartidesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('personatges_partides')->insert([
            'idPartida' => 1,
            'idHistoria' => 1,
            'idNarracio' => 1,
            'idPersonatge' => 1,
            'punts' => 50,
            'ultimaTirada' => 2,
            'acabada' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('personatges_partides')->insert([
            'idPartida' => 2,
            'idHistoria' => 2,
            'idNarracio' => 2,
            'idPersonatge' => 1,
            'punts' => 20,
            'ultimaTirada' => 6,
            'acabada' => false,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('personatges_partides')->insert([
            'idPartida' => 3,
            'idHistoria' => 3,
            'idNarracio' => 3,
            'idPersonatge' => 3,
            'punts' => 100,
            'ultimaTirada' => 4,
            'acabada' => true,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
