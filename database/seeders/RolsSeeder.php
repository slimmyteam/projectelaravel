<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class RolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('rols')->insert([
            'nom' => 'Administrador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('rols')->insert([
            'nom' => 'Narrador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('rols')->insert([
            'nom' => 'Jugador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
