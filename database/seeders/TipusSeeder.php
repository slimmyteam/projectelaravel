<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tipus')->insert([
            'nom' => 'Bàrbar',
            'descripcio' => "Per a alguns, la seva ràbia brolla de la comunió amb esperits d'animals salvatges. Uns altres recorren al seu hirviente reserva d'ira enfront d'un món ple de dolor. Per als bàrbars, la fúria és un poder que no sols els proporciona un frenesí cec en la batalla, sinó també extraordinaris reflexos, resistència i proeses de força.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Bard',
            'descripcio' => "Ja sigui un erudit, un poeta o un canalla, un bard teixeix la seva màgia a través de les seves paraules i la seva música per a inspirar als aliats, desmoralitzar als enemics, manipular ments, crear il·lusions i fins i tot sanar ferides.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Bruixot',
            'descripcio' => "Els bruixots són cercadors del coneixement que es troba amagat en el multivers. A través de pactes fets amb éssers poderosos de poder sobrenatural, els bruixots deslliguen efectes màgics tant subtils com espectaculars i recol·lecten secrets arcans per a potenciar el seu propi poder.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Clergue',
            'descripcio' => "Els clergues són intermediaris entre el món mortal i els distants plans divins. Tan diferents entre ells com els déus als quals serveixen, els clergues s'esforcen per personificar les obres de les seves deïtats. No són sacerdots ordinaris, un clergue es troba imbuït de màgia divina.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Druida',
            'descripcio' => "Ja sigui invocant a les forces elementals o emulant a les criatures del món animal, els *druidas són la personificació de la resistència, astúcia i fúria de la naturalesa. Diuen no tenir un domini sobre la naturalesa. En lloc d'això, es veuen com una extensió de la voluntat indomable d'aquesta.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Exploradora',
            'descripcio' => "Lluny de la bullícia de les ciutats i pobles, més enllà de les defenses que mantenen a les granges més llunyanes protegides dels terrors de la naturalesa, enmig d'espessos boscos sense camins i a través d'enormes i buides planes, els exploradors mantenen el seu interminable guàrdia.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Guerrer',
            'descripcio' => "Tots els guerrers comparteixen un domini magistral de les armes i armadures, i un exhaustiu coneixement de les habilitats del combat. A més, estan molt relacionats amb la mort, tant repartint-la com mirant-la fixament, desafiadores.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Embruixador',
            'descripcio' => "Els embruixadors tenen una màgia innata, conferida per una línia de sang exòtica, una influència d'un altre món o l'exposició a forces còsmiques desconegudes. Un no pot estudiar fetilleria com qui estudia un llenguatge, més del qual un pot aprendre a viure una vida llegendària. Ningú tria la fetilleria, el poder tria a l'embruixador.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Mag',
            'descripcio' => "Els mags són els practicants suprems de la màgia, definits i units com una classe pels encanteris que conjuren. A partir de la subtil ona de la màgia que impregna el cosmos, els mags llancen explosius encanteris de foc, arcs voltaics, subtils enganys i brutals formes de control mental.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Monje',
            'descripcio' => "Qualsevol que sigui la seva disciplina, els monjos estan units per la seva habilitat per a utilitzar màgicament l'energia que corre pels seus cossos. Ja sigui canalitzada en una impactant demostració de proesa marcial o en el subtil enfocament en l'habilitat defensiva i la velocitat, aquesta energia impulsa tot el que el monjo fa.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Paladí',
            'descripcio' => "Siguin quals siguin els seus orígens i les seves missions, els paladins estan units pels seus juraments per a lluitar en contra de les forces del mal. El jurament d'un paladí és un llaç molt poderós. És una font de poder que converteix a un devot guerrer en un campió beneït.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('tipus')->insert([
            'nom' => 'Murri',
            'descripcio' => "Els murris confien les seves habilitats, el sigil i les vulnerabilitats dels seus oponents per a aconseguir treure avantatge en qualsevol situació. Tenen un do per a trobar la solució a pràcticament qualsevol problema, demostrant un enginy i versatilitat, que és la pedra angular de qualsevol bon grup d'aventurers.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
