<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Rols;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        //Crear las tablas en la base de datos
        $this->call([RolsSeeder::class]);
        $this->call([UserSeeder::class]);
        $this->call([TipusSeeder::class]);
        $this->call([PersonatgesSeeder::class]);
        $this->call([HistoriesSeeder::class]);
        $this->call([PartidesSeeder::class]);
        $this->call([NarracionsSeeder::class]);
        $this->call([PersonatgesPartidesSeeder::class]);
    }
}
