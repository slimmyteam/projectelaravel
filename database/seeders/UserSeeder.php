<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Selene',
            'password' => Hash::make('super3'),
            'idRols' => 1,
            'email' => 'selene@correu.cat',
            'email_verified_at' => now(),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ana',
            'password' => Hash::make('super3'),
            'idRols' => 2,
            'email' => 'ana@correu.cat',
            'email_verified_at' => now(),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ethan',
            'password' => Hash::make('super3'),
            'idRols' => 3,
            'email' => 'ethan@correu.cat',
            'email_verified_at' => now(),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
