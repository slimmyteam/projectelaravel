<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonatgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('personatges')->insert([
            'idTipus' => 9,
            'idUser' => 1,
            'nom' => 'TheBestMage',
            'puntsInicials' => 5,
            'rerefons' => 'Rerefons de TheBestMage',
            'nivell' => 1,
            'rasa' => 'Elf',
            'militancia' => 'Militancia de TheBestMage',
            'presentacio' => 'presentacions/TheBestMage.txt',
            'foto' => 'images/elf.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('personatges')->insert([
            'idTipus' => 9,
            'idUser' => 2,
            'nom' => 'TheDruid',
            'puntsInicials' => 10,
            'rerefons' => 'Rerefons de TheDruid',
            'nivell' => 2,
            'rasa' => 'Draconid',
            'militancia' => 'Militancia de TheDruid',
            'presentacio' => 'presentacions/TheDruid.txt',
            'foto' => 'images/draconid.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('personatges')->insert([
            'idTipus' => 9,
            'idUser' => 3,
            'nom' => 'TheLegendaryWarrior',
            'puntsInicials' => 0,
            'rerefons' => 'Rerefons de TheLegendaryWarrior',
            'nivell' => 5,
            'rasa' => 'Tiefling',
            'militancia' => 'Militancia de TheLegendaryWarrior',
            'presentacio' => 'presentacions/TheLegendaryWarrior.txt',
            'foto' => 'images/tiefling.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('personatges')->insert([
            'idTipus' => 1,
            'idUser' => 1,
            'nom' => 'TheBarbar',
            'puntsInicials' => 7,
            'rerefons' => 'Rerefons de TheBarbar',
            'nivell' => 2,
            'rasa' => 'Tiefling',
            'militancia' => 'Militancia de TheBarbar',
            'presentacio' => 'presentacions/TheBarbar.txt',
            'foto' => 'images/gnome.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
