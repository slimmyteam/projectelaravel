<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('histories')->insert([
            'idUser' => 1,
            'titol' => 'Ella',
            'fitxer' => 'historias/Ella.pdf',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('histories')->insert([
            'idUser' => 2,
            'titol' => 'ElRobleDelPaso',
            'fitxer' => 'historias/ElRobleDelPaso.pdf',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('histories')->insert([
            'idUser' => 3,
            'titol' => 'LaSemillaDeLaVenganza',
            'fitxer' => 'historias/LaSemillaDeLaVenganza.pdf',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('histories')->insert([
            'idUser' => 1,
            'titol' => 'LaSucesionDeLordDevian',
            'fitxer' => 'historias/LaSucesionDeLordDevian.pdf',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
